<?php

if (! function_exists('url_model')) {
    /**
     * Generate a url for the application.
     *
     * @param  string  $path
     * @param  Model   model
     * @param  mixed   $parameters
     * @param  bool    $secure
     * @return string
     */
    function url_model($path = null,  $model = null, $parameters = [], $secure = null)
    {   $routeCollection = \Route::getRoutes();
        if($routeCollection->hasNamedRoute($path)){
            $route = $routeCollection->getByName($path);
            $params = [];
            if($model != null){
                if(count($route->parameterNames)==0){
                    $output_array = [];
                    preg_match_all("/\{\ *(.+?) *\??\}/", $route->uri, $output_array);
                    foreach ($output_array[1] as $key) {
                        if(isset($model[$key])){
                            $params[$key] = $model[$key];
                        }else if(isset($model->alias) && isset($model->alias[$key])){
                            $params[$key] = $model[$model->alias[$key]];
                        }                    
                    }
                }
                if($route->parameterNames){
                    foreach ($route->parameterNames as $key) {
                        if(isset($model[$key])){
                            $params[$key] = $model[$key];
                        }else if(isset($model->alias) && isset($model->alias[$key])){
                            $params[$key] = $model[$model->alias[$key]];
                        }                    
                    }
                }
            }
            $params = array_merge($params, $parameters);
            return url(route($path, $params, $secure));
        }

        return url($path, $params, $secure);
    }
}