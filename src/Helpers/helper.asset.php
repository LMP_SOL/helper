<?php 
if (! function_exists('asset_image')) {
    /**
     * Get image url for the application.
     *
     * @param  string  $name
     * @param  string  $file
     * @return string | null
     */
    function asset_image($name = null,  $file = '')
    {   $img = config('filesystems.image', false);
        
        if($img && isset($img[$name])){
            $img[$name][] = $file;
            return url('/', $img[$name]);
        }

        return null;
    }
}
if (! function_exists('real_image')) {
    /**
     * Get Real path for the application.
     *
     * @param  string  $name
     * @param  string  $file
     * @return string | null
     */
    function real_image($name = null,  $file = '')
    {
        $img = config('filesystems.image', false);
        
        if($img && isset($img[$name])){
            $img[$name][] = $file;
            return public_path() .'/'. implode('/', $img[$name]);
        }
        return null;
    }
}