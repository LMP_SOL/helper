<?php

namespace Lmpsolution\Helpers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        if(is_dir(realpath(dirname(__FILE__)) . '/Helpers/')){
            foreach (glob(realpath(dirname(__FILE__)) .'/Helpers/*.php') as $filename){
                require_once($filename);
            }
        }

        if(is_dir(app_path().'/Helpers/')){
            foreach (glob(app_path().'/Helpers/*.php') as $filename){
                require_once($filename);
            }
        }


    }
}
